
const level1 = '../assets/backgrounds/nature_1';
const level2 = '../assets/backgrounds/nature_2';

const pointerURLS = {
  defaultPointer: '../assets/pointers/10-23-21/ergonomic-cursors/arrow-diagonal/default',
  diamondPointer: '../assets/pointers/10-23-21/ergonomic-cursors/diamond-round/default',
};

const gameAssetsURL = {
  cubes: '../assets/cubes',
};

export const backgroundMap = {
  backgrounds1: [
    {name: 'blue_sky', url: `${level1}/1.png`},
    {name: 'clouds', url: `${level1}/2.png`},
    {name: 'back_mountains', url: `${level1}/3.png`},
    {name: 'grass', url: `${level1}/5.png`},
    {name: 'back_tree', url: `${level1}/6.png`},
    {name: 'front_tree', url: `${level1}/7.png`},
    {name: 'front_grass', url: `${level1}/8.png`},
    {name: 'land', url: `${level1}/10.png`},
  ],
  backgrounds2: [
    {name: 'blue_sky', url: `${level2}/1.png`},
    {name: 'clouds', url: `${level2}/2.png`},
    {name: 'back_mountains', url: `${level2}/3.png`},
    {name: 'grass', url: `${level2}/4.png`},
  ],
};

export const cursorMap = {
  diagonal: [
    {name: 'blackPointer',
      url: `${pointerURLS.defaultPointer}/Black_Arrow.png`},
  ],
};

export const blockMap = {
  cubesColored: [
    {name: 'blue_cube', url: `${gameAssetsURL.cubes}/blue_cube.png`},
    {name: 'green_cube', url: `${gameAssetsURL.cubes}/green-cube.png`},
    {name: 'purple_cube', url: `${gameAssetsURL.cubes}/purple-cube.png`},
    {name: 'red_cube', url: `${gameAssetsURL.cubes}/red-cube.png`},
    {name: 'yellow_cube', url: `${gameAssetsURL.cubes}/yellow-cube.png`},
  ],
  board: [
    {name: 'board_background', url: '../assets/layouts/gameField-background.png'},
  ],
};
