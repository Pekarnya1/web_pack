import {Application, Graphics} from 'pixi.js';
import {backgroundMap, cursorMap, blockMap} from './textureMap.js';
import {createBackground, animateBackground} from './puzle/backgroundConf.js';
import {configPointer, backSoundPlay} from './puzle/conf/gameConfig.js';
import '../assets/styles/pointerStyles.css';
import {Board} from './puzle/game/board.js';


// Start playing the back sound
// N.B it is not necessary to place sound into the game loop
backSoundPlay;

const app = new Application({
  width: window.innerWidth,
  height: window.innerHeight,
});

// Hide the overflow
document.body.style.overflow = 'hidden';

app.screen.width = window.innerWidth;

document.body.appendChild(app.view);
app.stage.interactive = true;

backgroundMap.backgrounds1.forEach((value) => app.loader.add(
    value.name, value.url),
);

cursorMap.diagonal.forEach((value) => app.loader.add(
    value.name, value.url),
);

blockMap.cubesColored.forEach((value) => app.loader.add(
    value.name, value.url,
));

blockMap.board.forEach((value) => app.loader.add(
    value.name, value.url,
));


const runGame = () => {
  // creating marker
  const marker = new Graphics();
  marker.beginFill(0xff00, 1);
  marker.drawCircle(0, 0, 10, 10);
  marker.endFill;

  const background = createBackground(window.innerWidth);

  console.log(background);

  app.stage.addChild(background.view);
  app.stage.addChild(marker);


  // Configuration of the custom pointer
  const pointer = configPointer('default', app);
  pointer.setPointer();

  console.log(app.stage);

  animateBackground(background, app);
  const board = new Board(app);
};
app.loader.load(runGame);
