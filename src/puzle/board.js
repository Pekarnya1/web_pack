export class Board {
  constructor(rows, cols, pieceClass) {
    this.rows = rows;
    this.cols = cols;
    this.pieces = [];

    // Initialize the board with random pieces
    for (let i = 0; i < rows; i++) {
      const row = [];
      for (let j = 0; j < cols; j++) {
        const piece = new pieceClass(); // Create a new piece object
        row.push(piece);
      }
      this.pieces.push(row);
    }
  }
}


