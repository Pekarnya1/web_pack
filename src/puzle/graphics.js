
import {Container, Sprite, Texture, TilingSprite} from 'pixi.js';

/**
 * Create container background filled with specified sprites.
 * N.B: Call it only via {BackgroundBuilder} constructor
 * @param {object} spriteInstances - object that contains
 * the keys of layers and the name from textureMap
 * @return {Container} Container filled with the sprites.
 */
export class Background {
  // eslint-disable-next-line require-jsdoc
  constructor(spriteInstances) {
    // Create a new background container
    this._view = new Container();

    this.spriteInstances = spriteInstances;

    this.instancesKeys = Object.keys(this.spriteInstances);
    console.log(this.instancesKeys);

    this._textureMap = {};
    this._spriteMap = {};

    this.#texturePack();
  }

  /**
 * Method to collect sprites and place them
 * into the container as child.
 * This method is using exemplars of TextureCreator class
 */
  #texturePack() {
    this.instancesKeys.forEach((key) => {
      this._textureMap[key] = new TextureCreator(
          this.spriteInstances[key],
          window.innerWidth,
          324,
      );
      this._spriteMap[key] = this._textureMap[key].createTilingSprite(false);
      this._view.addChild(this._spriteMap[key]);
    });
  }


  /**
  * Create sprite instance depends on flag value.
  * @return {Container} Container filled with sprites.
  */
  get view() {
    // Add the sprites into the container
    return this._view;
  }
}

/**
  * Helps to configure the background via the builder
  * pattern.
  * @param {number} window.Width - size of the window.
  */
export class BackgroundBuilder {
  /**
   * @param {number} windowWidth - now it doesnt have any effect
   */
  constructor(windowWidth) {
    this.windowWidth = windowWidth;
    this.spriteInstances = {};
  }

  /**
  * Continuously builds the object of the background
  * configuration
  * @param {string} object - The string containing the name of the object
  * to build.
  * @param {string} name - The name of the object thar is
  * already defined in background map.-
  * @return {object} Sprite object with key to manipulate
  * and value to adress co bsckground map.
  * @example
  * // add new background object N.B name parametr is used from texture map!!!
  * // You can  name object name whatever you want.
  * const background = new BackgroundBuilder(width)
      .addObject('baseTexture', 'blue_sky')
  */
  addObject(object, name) {
    this.spriteInstances[object] = name;
    return this;
  }

  /**
  * Creates final build of the background object.
  * @return {Background} new Background.
  */
  build() {
    return new Background(this.spriteInstances);
  }
}

/**
  * Wrapper for the PIXI sprite and texture classes.
  * @param {string} spriteName - The string containing
  * the name of the object
  * @param {number} width - width of the sprite.
  * @param {number} height - height of the sprite
  * @return {object} Sprite object with key to manipulate
  * and value to adress co bsckground map.
  * @example
  * // texture of the class object:
  * const textureObj = new TextureCreator(
  *   'path/to/file.png', 10, 10);
  * const texture = textureObj.texture;
  */
export class TextureCreator {
  // eslint-disable-next-line require-jsdoc
  constructor(spriteName, width, height) {
    this.spriteContainer = new Container();
    this.spriteName = spriteName;
    console.log(this.spriteName);

    this.texture = Texture.from(this.spriteName);
    this.width = width;
    this.height = height;
  }

  /**
 * Create sprite instance depends on flag value.
 * @param {boolean} isFit - The switcher beetween sprites that
 * have to fit the screen
 * @return {Sprite} Sprite instance.
 * @example
 * // sprite of the object that fits the screen
 * const object = new TextureCreator(
  *   'path/to/file.png', 10, 10);
* const sprite = object.createSprite(true);
 */
  createSprite(isFit) {
    const sprite = new Sprite(this.texture);

    // Check if the sprite shoud fit the screen
    if (isFit) {
      // document.body.clientWidth.
      sprite.width = document.body.clientWidth;
      sprite.height = document.body.clientHeight;
    }

    sprite.width = this.width;
    sprite.height = this.height;

    sprite.position.set(0, 0);
    return sprite;
  }

  /**
 * Create tailing sprite.
 * N.B. In dev version to prevent some bugs
 * when tiling sprite repeating in all axis directions
 * Method use sprite.scale.set
 * @return {TilingSprite} examplar of the PIXI.TilingSprite.
 */
  createTilingSprite() {
    const tilingSprite = new TilingSprite(
        this.texture,
        this.width,
        this.height,
    );
    tilingSprite.position.set(0, 0);
    tilingSprite.scale.set(
        document.body.clientHeight/this.height,
    );
    return tilingSprite;
  }
}

