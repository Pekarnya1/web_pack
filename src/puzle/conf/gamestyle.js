

/**
 * Create pointer style.
 * N.B: Call it only via {BackgroundBuilder} constructor
 * @param {object} cursorInstance - object that contains
 * @param {Object} app current aplication
 * the keys of layers and the name from textureMap
 */
export class Pointer {
  // eslint-disable-next-line require-jsdoc
  constructor(setting, app) {
    this.setting = setting;
    this.app = app;
  }

  /**
 * Set the pointer style depend on setting preference
 */
  setPointer() {
    if (this.setting === 'default') {
      this.#setDefaultPointer();
    }
  }
  /**
 * Set default pointer style.
 * via just changing the document.body style.
 */
  #setDefaultPointer() {
    document.body.classList.add('default-cursor');
  }
}

/**
  * Continuously builds the object of the pointer
  * configuration
  * @param {string} setting - string defenition of
  * @param {Object} app current application
  * the configuration
  */
export class PointerBuilder {
  // eslint-disable-next-line require-jsdoc
  constructor(setting, app) {
    this.app = app;
    this.setting = setting;
    this.cursorInstance = {};
  }

  /**
  * Continuously builds the object of the pointer
  * configuration
  * @param {string} object - The string containing the name of the object
  * to build.
  * @param {string} name - The name of the object thar is
  * already defined in texture map.-
  * @return {object} Sprite object with key to manipulate
  * and value to adress co bsckground map.
  */

  /**
  * Creates final build of the Pointer object.
  * @return {Pointer} new Pointer.
  */
  build() {
    return new Pointer(this.setting, this.app);
  }
}
