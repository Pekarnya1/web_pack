import {Container, filters} from 'pixi.js';
import {boardConfig} from '../conf/gameConfig';
import {Cube} from './field';
import {blockMap} from '../../textureMap';

/**
 * Representation of the board instance with it container
 */
export class Board {
  // eslint-disable-next-line require-jsdoc
  constructor(app) {
    this.app = app;
    this.container = new Container();
    this.fields = [];
    this.rows = boardConfig.rows;
    this.cols = boardConfig.cols;
    this.create();
  }

  // eslint-disable-next-line require-jsdoc
  create() {
    this.createFields();
  }

  // eslint-disable-next-line require-jsdoc
  createFields() {
    for (let row = 0; row < this.rows; row++) {
      for (let col = 0; col < this.cols; col++) {
        this.createField(row, col);
      }
    }
  }

  // eslint-disable-next-line require-jsdoc
  createField(row, col) {
    const cube = new Cube(row, col, this.#randomizeBlock());
    this.fields.push(cube);
    this.container.addChild(cube.sprite);
    this.#adjustPosition();
    this.app.stage.addChild(this.container);
  }

  // eslint-disable-next-line require-jsdoc
  #adjustPosition() {
    this.fieldSize = this.fields[0].sprite.width;
    this.width = this.cols * this.fieldSize;
    this.height = this.cols * this.fieldSize;
    this.container.x = (this.app.screen.width - this.width) / 2 + this.fieldSize / 2;
    this.container.y = (this.app.screen.height - this.height) * 0.618 + this.fieldSize * 0.618;
  }

  // eslint-disable-next-line require-jsdoc
  #randomizeBlock() {
    const randomIndex = Math.floor(Math.random() * blockMap.cubesColored.length);
    const randomName = blockMap.cubesColored[randomIndex].name;
    return randomName;
  }
}

// Transparent sprite
// this.background = new TextureCreator('board_background', 400, 400);
// this.backSprite = this.background.createSprite(this.background);
// const filterGreen = new filters.ColorMatrixFilter();
// this.backSprite.filters = [filterGreen];
// filterGreen.colorTone(22, 55, 0, 0, true);
// this.container.addChild(this.backSprite);
