import {TextureCreator} from '../graphics.js';
import * as PIXI from 'pixi.js';

/**
 * Describes the rulesand methods for
 * working with the field (single square) instance on the board
 * @param {number} row current row of the board sector.
 * @param {number} col current column of the board sector.
 * @param {string} spriteName name of the sprite
 * N.B I`m using TextureMap but if you dont, you have to pass
 * the relative or absolute path to the image!
 * @example
 * // Add sprite of cube:
 * const cube = new Cube(10, 10, 'blue_cube');
  app.stage.addChild(cube.sprite);
*/
export class Cube {
  // eslint-disable-next-line require-jsdoc
  constructor(row, col, spriteName) {
    this.spriteName = spriteName;
    this.row = row;
    this.col = col;
    this.textureObj = new TextureCreator(this.spriteName, 50, 50);
    this.sprite = this.textureObj.createSprite(false);
    this.sprite.position.set(this.position.x, this.position.y);
    this.sprite.interactive = true;
    this.sprite.on('pointerdown', (even) => {
      console.log(even);
      const filterGreen = new PIXI.filters.ColorMatrixFilter();
      this.sprite.filters = [filterGreen];
      filterGreen.lsd(false);
      // transparent filterGreen.colorTone(22, 55, 5, 4, true);
    });
    // this.sprite.anchor.set(0.5);
    console.log(`${this.sprite}`);
  }

  /**
 * Get the position of the cube
*/
  get position() {
    return {
      x: this.col * 50,
      y: this.row * 50,
    };
  }

  // eslint-disable-next-line require-jsdoc
  setTile(tile) {
    this.tile = tile;
    tile.field = this;
    tile.setPosition(this.position);
  }
}
